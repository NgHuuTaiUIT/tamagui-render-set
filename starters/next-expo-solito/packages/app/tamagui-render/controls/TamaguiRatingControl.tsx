import { withJsonFormsControlProps } from '@jsonforms/react';
import { Rating } from '../tamagui-controls/Rating';
import { rankWith, scopeEndsWith } from '@jsonforms/core';

interface RatingControlProps {
  data: any;
  handleChange(path: string, value: any): void;
  path: string;
}

const TamaguiRatingControl = ({ data, handleChange, path }: RatingControlProps) => (
  <Rating
    value={data}
    updateValue={(newValue: number) => handleChange(path, newValue)}
  />
);
export const tamaguiRatingControlTester: RankedTester = rankWith(
    3, //increase rank as needed
    scopeEndsWith('rating')
  );

export default withJsonFormsControlProps(TamaguiRatingControl);