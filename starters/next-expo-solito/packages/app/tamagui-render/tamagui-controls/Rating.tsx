import React from 'react';
import { useState } from 'react';
import { InputLabel } from '@mui/material';
import { YStack, XStack, Label, styled } from 'tamagui';
import { Star, View } from '@tamagui/lucide-icons'
interface RatingProps {
  id?: string;
  value: number;
  updateValue: (newValue: number) => void;
}

export const Rating: React.FC<RatingProps> = ({ id, value, updateValue }) => {
  const [hoverAt, setHoverAt] = useState<number | null>(null);

  return (
    <YStack id='#/properties/rating' className='rating'>
      <Label>Rating</Label>
      <XStack cursor='pointer'>
        {[0, 1, 2, 3, 4].map((i) => {
          const fullStars = hoverAt ?? value;

          return (
            <span
              onMouseOver={() => setHoverAt(i + 1)}
              onMouseOut={() => setHoverAt(null)}
              onClick={() => updateValue(i + 1)}
              key={`${id}_${i}`}
            >
              {i < fullStars ? '\u2605' : '\u2606'}
              {/* <IconStar fill='white'color='white' /> */}
            </span>
          );
        })}
      </XStack>
    </YStack>
  );
};
