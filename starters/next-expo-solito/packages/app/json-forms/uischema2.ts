export default {
  "type": "VerticalLayout",
  "elements": [
    {
      "type": "HorizontalLayout",
      "elements": [
        {
          "type": "Control",
          "scope": "#/properties/name"
        },
        {
          "type": "Control",
          "scope": "#/properties/personalData/properties/age"
        },
        {
          "type": "Control",
          "scope": "#/properties/birthDate"
        },
      ]
    },
    {
      "type": "HorizontalLayout",
      "elements": [
        {
          "type": "Control",
          "scope": "#/properties/personalData/properties/height"
        },
        {
          "type": "Control",
          "scope": "#/properties/nationality"
        },
        {
          "type": "Control",
          "scope": "#/properties/occupation",
          "suggestion": [
            "Accountant",
            "Engineer",
            "Freelancer",
            "Journalism",
            "Physician",
            "Student",
            "Teacher",
            "Other"
          ]
        }
      ]
    },
    {
      "type": "HorizontalLayout",
      "elements": [
        {
          "type": "Control",
          "scope": "#/properties/radioGroup",
          "options": {
            "format": "radio"
          }
        },
        {
          "type": "Control",
          "scope": "#/properties/multilineString",
          "options": {
            "multi": true
          }
        },
        {
          "type": "Control",
          "scope": "#/properties/toggle",
          "label": "Boolean as Toggle",
          "options": {
            "toggle": true
          }
        }
        // {
        //   "type": "Control",
        //   "scope": "#/properties/rating"
        // }
      ]
    },
    {
      "type": "VerticalLayout",
      "elements": [
        {
          "type": "Control",
          "scope": "#/properties/slider",
          "options": {
            "slider": true
          }
        },
      ]
    },
  ]
}