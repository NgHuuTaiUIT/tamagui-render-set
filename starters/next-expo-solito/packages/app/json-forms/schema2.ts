export default {
  "type": "object",
  "properties": {
    "name": {
      "type": "string",
      "minLength": 3,
      "description": "Please enter your name"
    },
    "vegetarian": {
      "type": "boolean",
    },
    "birthDate": {
      "type": "string",
      "format": "date",
    },
    "nationality": {
      "type": "string",
      "oneOf": [
        {
          "const": "foo",
          "title": "Foo"
        },
        {
          "const": "bar",
          "title": "Bar"
        },
        {
          "const": "foobar",
          "title": "FooBar"
        }
      ]
    },
    "personalData": {
      "type": "object",
      "properties": {
        "age": {
          "type": "integer",
          "description": "Please enter your age."
        },
        "height": {
          "type": "number",
        },
        "drivingSkill": {
          "type": "number",
          "maximum": 10,
          "minimum": 1,
          "default": 7
        }
      },
      "required": [
        "age",
        "height"
      ]
    },
    "occupation": {
      "type": "string",
    },
    "postalCode": {
      "type": "string",
      "maxLength": 5
    },
    "radioGroup": {
      "type": "string",
      "enum": [
        "foo",
        "bar",
        "foobar"
      ],
    },
    "multilineString": {
      "type": "string",
      "description": "Multiline Example"
    },
    "slider": {
      "type": "number",
      "minimum": 1,
      "maximum": 5,
      "default": 2,
      "description": "Slider Example"
    },
    "rating": {
      "type": "integer",
      "minimum": 0,
      "maximum": 5
    },
    "toggle": {
      "type": "boolean",
      "description": "The \"toggle\" option renders boolean values as a toggle."
    }
  },
  "required": [
    "occupation",
    "nationality"
  ]
}